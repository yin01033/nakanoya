<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', '_nakanoya2015tmp');

/** MySQL データベースのユーザー名 */
define('DB_USER', '_nakanoya2015tmp');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'nakanoya2015db');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql503.heteml.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',_)I8VJ2dDkmCCuvYS1b5pAKDm;0NIhXgo9fAJY7q}k/eU8](0y#g>M$5c4I%b9l');
define('SECURE_AUTH_KEY',  'G9HlJ9i@|ID67C?$])&2{S6[:0e?Se53eCe)CD|dqfan!tr^1w<=F.JU$*oWnkPH');
define('LOGGED_IN_KEY',    '(/PJRr)|Mh%ORvlIi2a*%%d*.Hez-:|>|rbsYi,&G:T19`~j=4r]8KA,w998/|<L');
define('NONCE_KEY',        '*s_hREm1T{sTW.]n(%9%PA:BA:~NLQ3T}=@tW7l+_Tduf0Xl4XqJ25p]+*kVZd+1');
define('AUTH_SALT',        'u~lG|MQV9/Fg$7I7V>HN/<k^`Ksy)1q2[S~S9t*97Of9M*8!{FPJW?gvePkmGQeb');
define('SECURE_AUTH_SALT', '$8:#r^#olip=qn?(/iQk2=WWg{~(r7iu5xwn,ySwW|$C&]J?533x,PKrqnxXlWHh');
define('LOGGED_IN_SALT',   'J^LU.8Gi`K&BhOUcHOQS85~oj=BLnm.=9Yl.W.vwW0=^Emzq[G}?Bc7&t}QX&do&');
define('NONCE_SALT',       '[Q>sc$e?3wM}ZC*umH%*%!/xAeLF|@KTlcBEg9XZx$2NF!}.JC[NEEBi$q)A!Sym');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

add_filter('xmlrpc_enabled', '__return_false');

add_filter('xmlrpc_methods', function($methods) {
    unset($methods['pingback.ping']);
    unset($methods['pingback.extensions.getPingbacks']);
    return $methods;
});