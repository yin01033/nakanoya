<?php
/**
 * The default template for displaying content
 *
 * @package TC_Custom
 */
?>
    <div class="blog-post-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <div class="post-thumbnail">
                    <a href="<?php the_permalink(); ?>">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <?php the_post_thumbnail(); ?>
                        <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>\images\default_blog_image@2x.png"/>
                        <?php } ?>
                    </a>
                    <div class="blog-post-title">
                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentyeleven' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                    </div>
                </div>
            </header><!-- .entry-header -->
	</article><!-- #post-<?php the_ID(); ?> -->
    </div>
