<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package TC_Custom
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">
                    <!--
			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				//if ( ! is_404() ) get_sidebar( 'footer' );
			?>
			<div id="site-generator">
				<?php //do_action( 'twentyeleven_credits' ); ?>
				<a href="<?php //echo esc_url( __( 'http://wordpress.org/', 'twentyeleven' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentyeleven' ); ?>"><?php //printf( __( 'Proudly powered by %s', 'twentyeleven' ), 'WordPress' ); ?></a>
			</div>
                    -->
                   
            <?php if (!is_front_page()) { ?>
                <img class="footer-image" src="<?php echo get_template_directory_uri(); ?>/images/footer-image@2x.jpg"/>
            <?php } ?>
            
            <div id="supplementary" class="two">
                <div id="first" class="widget-area" role="complementary">
                    <div class="footer-column">
                        <div class="footer-column-left">メニュー</div>
                        <div class="footer-column-right">
                            <ul>
                            <?php
                            	$menu = wp_get_nav_menu_object ('メインメニュー');
                                $menu_items = wp_get_nav_menu_items($menu->term_id);                                
                                foreach ($menu_items as $menu_item) {
                                    $menupost = get_post($menu_item->object_id);
                                    echo '<li><a href="'.$menu_item->url.'">'.$menupost->post_title.'</a></li>';
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="third" class="widget-area" role="complementary">
                    <div class="footer-column">
                        <div class="footer-column-left">ブログ</div>
                        <div class="footer-column-right">
                            <ul>
                                <?php
                                    $args = array( 'numberposts' => '7' );
                                    $recent_posts = wp_get_recent_posts( $args );
                                    foreach( $recent_posts as $recent ){
                                        echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="site-generator">© 2015 大分の椎茸　ゆるぎ椎茸の中野屋</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>