/* 
 * Common javascript used in theme
 */

(function ($) {
    
    $(window).scroll( function(){
        $('img').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
                $(this).animate({'opacity':'1'},500);
            }
        });
    });
    
    $(window).resize(function() {
        if ($(window).width() > 767) {
            $('nav#access').show();
        } else {
            $('nav#access').hide();
        }
    });
    
    $(document).ready(function() {
       $('.mobile-menu').click(function() {
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
                $(this).html('<span class="chevron bottom"></span> メニュー');
                $('nav#access').slideUp();
            } else {
                $(this).addClass('opened');
                $(this).html('<span class="chevron up"></span> 閉じる');
                $('nav#access').slideDown();
            }
        }); 
    });
    
})(jQuery);


